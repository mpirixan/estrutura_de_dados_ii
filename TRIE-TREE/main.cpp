#include <iostream>
#include <bits/stdc++.h>
using namespace std;

class Trie{
  public:
  Trie *filho [26];
  int palavras;
  int valor(char c){
    return c-'a';
  }

  Trie(){
    for(int i = 0; i <26; i++) filho[i] = NULL;
    palavras = 0;
  }

  //estrutura de inserção
  void inserir(const string &palavra, int i=0){
    if(i >=palavra.size()) {
      palavras++;
      return;
    }
    int id = valor(palavra[i]);
    if(filho[id] == NULL) filho[id] = new Trie();

    filho[id]->inserir(palavra, i+1);
  }

  //estrutura de busca, o valor retornado sera 1 para True = encontrada e 0 para False = não encontrado.
  bool busca (const string palavra, int i=0){
    if(i >=palavra.size()) {
      return palavras; 
    }
    int id = valor(palavra[i]);
    if(filho[id] == NULL) return 0;
    else return filho[id]->busca(palavra,i+1);
  }

  //estrutura de inserção
  void remover(const string &palavra, int i=0){
    if(i >=palavra.size()) {
      palavras--;
      return;
    }
    int id = valor(palavra[i]);
    if(filho[id] == NULL) filho[id] = new Trie();

    filho[id]->remover(palavra, i+1);
  }

};

int main(){
    Trie *obj = new Trie();

    obj -> inserir ("Teste");
    obj -> inserir ("tester");
    cout << obj->busca("Teste") << endl;
    cout << obj->busca("Tester") << endl;
    obj -> remover ("Teste");
    cout << obj->busca("teste") << endl;
  
  }
