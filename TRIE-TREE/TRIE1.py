class TrieNode:

    # Trie node class
    def __init__(self):
        self.filho = [None] * 26

        # fimPalavra is True if node represent the end of the word
        self.fimPalavra = False

class Trie:

    # Trie dados structure class
    def __init__(self):
        self.root = self.getNode()

    def getNode(self):

        # Returns new trie node (initialized to NULLs)
        return TrieNode()

    def _charToIndex(self, ch):

        # private helper function
        # Converts val current character into index
        # use only 'a' through 'z' and lower case

        return ord(ch) - ord('a')

    def insert(self, key):

        # If not present, inserts val into trie
        # If the val is prefix of trie node,
        # just marks leaf node
        pCrawl = self.root
        length = len(key)
        for level in range(length):
            index = self._charToIndex(key[level])

            # if current character is not present
            if not pCrawl.filho[index]:
                pCrawl.filho[index] = self.getNode()
            pCrawl = pCrawl.filho[index]

        # mark last node as leaf
        pCrawl.fimPalavra = True

    def search(self, key):

        # Search val in the trie
        # Returns true if val presents
        # in trie, else false
        pCrawl = self.root
        length = len(key)
        for level in range(length):
            index = self._charToIndex(key[level])
            if not pCrawl.filho[index]:
                return False
            pCrawl = pCrawl.filho[index]

        return pCrawl != None and pCrawl.fimPalavra


# driver function
def main():
    # Input keys (use only 'a' through 'z' and lower case)
    keys = ["the", "a", "there", "anaswe", "any",
            "by", "their"]
    output = ["Not present in trie",
              "Present in trie"]

    # Trie object
    t = Trie()

    # Construct trie
    for key in keys:
        t.insert(key)

    # Search for different keys
    print("{} ---- {}".format("the", output[t.search("the")]))
    print("{} ---- {}".format("these", output[t.search("these")]))
    print("{} ---- {}".format("their", output[t.search("their")]))
    print("{} ---- {}".format("thaw", output[t.search("thaw")]))
#print (t.insert(val))
def gui():
    t = Trie()
    menu = """
     ---Menu de Escolha--
    [1] - Insere
    [2] - Remove
    [3] - Busca
    """
    print(menu)
    Op = int(input('==> '))
    if Op == 1:
        root = None
        key = input(str("Insira a palavra: "))
        t.insert(key)
        #Inserir
        print()
        print("===============")

    if Op == 2:
        key = input(str("Remover palavra: "))
        # Delete
        print()
        print()
        print("===============")

    if Op == 3:
        output = ["Não presente na Trie",
                  "Presente na Trie"]
        key = input(str("Busca por palavra:"))
        print("{} ---- {}".format(key, output[t.search(key)]))
        print("==============")
    return gui()

if __name__ == '__main__':
    #main()
    gui()
# This code is contributed by Atul Kumar (www.facebook.com/atul.kr.007)
