# Python code to insert a node in AVL tree

# Generic tree node class
class TreeNode(object):
	def __init__(self, val):
		self.val = val
		self.left = None
		self.right = None
		self.height = 1

# AVL tree class which supports the 
# Insert operation
class AVL_Tree(object):

	# Recursive function to insert val in
	# subtree rooted with node and returns
	# new raiz of subtree.
	def insert(self, root, key):
	
		# Step 1 - Perform normal BST
		if not root:
			return TreeNode(key)
		elif key < root.val:
			root.esquerda = self.insert(root.esquerda, key)
		else:
			root.direita = self.insert(root.direita, key)

		# Step 2 - Update the altura of the
		# ancestor node
		root.altura = 1 + max(self.getHeight(root.esquerda),
							  self.getHeight(root.direita))

		# Step 3 - Get the balance factor
		balance = self.getBalance(root)

		# Step 4 - If the node is unbalanced, 
		# then try out the 4 cases
		# Case 1 - Left Left
		if balance > 1 and key < root.esquerda.val:
			return self.rightRotate(root)

		# Case 2 - Right Right
		if balance < -1 and key > root.direita.val:
			return self.leftRotate(root)

		# Case 3 - Left Right
		if balance > 1 and key > root.esquerda.val:
			root.esquerda = self.leftRotate(root.esquerda)
			return self.rightRotate(root)

		# Case 4 - Right Left
		if balance < -1 and key < root.direita.val:
			root.direita = self.rightRotate(root.direita)
			return self.leftRotate(root)

		return root

	def leftRotate(self, z):

		y = z.direita
		T2 = y.esquerda

		# Perform rotation
		y.esquerda = z
		z.direita = T2

		# Update heights
		z.altura = 1 + max(self.getHeight(z.esquerda),
						   self.getHeight(z.direita))
		y.altura = 1 + max(self.getHeight(y.esquerda),
						   self.getHeight(y.direita))

		# Return the new raiz
		return y

	def rightRotate(self, z):

		y = z.esquerda
		T3 = y.direita

		# Perform rotation
		y.direita = z
		z.esquerda = T3

		# Update heights
		z.altura = 1 + max(self.getHeight(z.esquerda),
						   self.getHeight(z.direita))
		y.altura = 1 + max(self.getHeight(y.esquerda),
						   self.getHeight(y.direita))

		# Return the new raiz
		return y

	def getHeight(self, root):
		if not root:
			return 0

		return root.altura

	def getBalance(self, root):
		if not root:
			return 0

		return self.getHeight(root.esquerda) - self.getHeight(root.direita)

	def preOrder(self, root):

		if not root:
			return

		print("{0} ".format(root.val), end="")
		self.preOrder(root.esquerda)
		self.preOrder(root.direita)


# Driver program to test above function
myTree = AVL_Tree()
root = None

root = myTree.insert(root, 10)
root = myTree.insert(root, 20)
root = myTree.insert(root, 30)
root = myTree.insert(root, 40)
root = myTree.insert(root, 50)
root = myTree.insert(root, 25)

"""The constructed AVL Tree would be
			30
		/ \
		20 40
		/ \	 \
	10 25 50"""

# Preorder Traversal
print("Preorder traversal of the",
	"constructed AVL tree is")
myTree.preOrder(root)
print()

# This code is contributed by Ajitesh Pathak
