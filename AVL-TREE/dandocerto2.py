# Criação de arvore linha 4
# Inserção linha 14 e 183
# Exclusão de Linha 54 e 196
# Geração de classe AVL padrão sem nós
class TreeNode(object):
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None
        self.height = 1


class AVL_Tree(object):

    def insert(self, root, key):

        # Passo 1 - Formando regras de BST
        if not root:
            return TreeNode(key)
        elif key < root.val:
            root.left = self.insert(root.left, key)
        else:
            root.right = self.insert(root.right, key)

        # Passo 2 - Atualização de altura em
        # relação ao anterior
        root.height = 1 + max(self.getHeight(root.left),
                              self.getHeight(root.right))

        # Passo 3 - Balancear a Arvore
        balance = self.getBalance(root)

        # Passo 4  - Caso a AVL seja desbalanceada,
        # o algoritmo terá 4 casos de resolusão.
        # Caso 1 - Esquerda Esquerda
        if balance > 1 and key < root.left.val:
            return self.rightRotate(root)

        # Caso 2 - Direita Direita
        if balance < -1 and key > root.right.val:
            return self.leftRotate(root)

        # Caso 3 - Esquerda Direita
        if balance > 1 and key > root.left.val:
            root.left = self.leftRotate(root.left)
            return self.rightRotate(root)

        # Caso 4 - Direita Esquerda
        if balance < -1 and key < root.right.val:
            root.right = self.rightRotate(root.right)
            return self.leftRotate(root)

        return root

    # Função recursiva para deletar o nó com
    # o valor dado da sub arvore com a raiz dada.
    # Retorna a raiz da sub arovre, modificada.
    def delete(self, root, key):

        # Passo 1 - Delete padrão de BST
        if not root:
            return root

        elif key < root.val:
            root.left = self.delete(root.left, key)

        elif key > root.val:
            root.right = self.delete(root.right, key)

        else:
            if root.left is None:
                temp = root.right
                root = None
                return temp

            elif root.right is None:
                temp = root.left
                root = None
                return temp

            temp = self.getMinValueNode(root.right)
            root.val = temp.val
            root.right = self.delete(root.right,
                                     temp.val)

        # Caso a arvore possui apenas um nó,
        # Simplesmente retorna a raiz
        if root is None:
            return root

        # Passo 2 - Atualiza altura em relação ao
        # nó anterior
        root.height = 1 + max(self.getHeight(root.left),
                              self.getHeight(root.right))

        # Passo 3 - Balancea a BST
        balance = self.getBalance(root)

        # Passo 4 - Se o nó está desbalanceado,
        # Terá 4 casos de resolução
        # Caso 1 - Esquerda Esquerda
        if balance > 1 and self.getBalance(root.left) >= 0:
            return self.rightRotate(root)

        # Caso 2 - Direita Direita
        if balance < -1 and self.getBalance(root.right) <= 0:
            return self.leftRotate(root)

        # Caso 3 - Esquerda Direita
        if balance > 1 and self.getBalance(root.left) < 0:
            root.left = self.leftRotate(root.left)
            return self.rightRotate(root)

        # Caso 4 - Direita Esquerda
        if balance < -1 and self.getBalance(root.right) > 0:
            root.right = self.rightRotate(root.right)
            return self.leftRotate(root)

        return root

    def leftRotate(self, z):

        y = z.right
        T2 = y.left

        # Rotação
        y.left = z
        z.right = T2

        # Atualização de Altura
        z.height = 1 + max(self.getHeight(z.left),
                           self.getHeight(z.right))
        y.height = 1 + max(self.getHeight(y.left),
                           self.getHeight(y.right))

        # Retorna nova raíz
        return y

    def rightRotate(self, z):

        y = z.left
        T3 = y.right

        # Realiza rotação
        y.right = z
        z.left = T3

        # Atualização de altura
        z.height = 1 + max(self.getHeight(z.left),
                           self.getHeight(z.right))
        y.height = 1 + max(self.getHeight(y.left),
                           self.getHeight(y.right))

        # Retorna nova raíz
        return y

    def getHeight(self, root):
        if not root:
            return 0

        return root.height

    def getBalance(self, root):
        if not root:
            return 0

        return self.getHeight(root.left) - self.getHeight(root.right)

    def getMinValueNode(self, root):
        if root is None or root.left is None:
            return root

        return self.getMinValueNode(root.left)

    def preOrder(self, root):

        if not root:
            return

        print("{0} ".format(root.val), end="")
        self.preOrder(root.left)
        self.preOrder(root.right)
root = None
raiz = None
lnums = []
lista = lnums
def gui():
    global root
    global raiz
    global lnums
    global lista
    avlArvore = AVL_Tree()
    menu = """
     ---Menu de Escolha--
    [1] - Insere
    [2] - Remove
    [3] - Imprimi
    """
    print(menu)
    Op = int(input('==> '))
    if Op == 1:
        root = None
        num = input("Insira o número: ")
        lnums.append(num)
        # Inserir
        #root = None
        #nums = [9, 5, 10, 0, 6, 11, -1, 1, 2]
        #if raiz != None:
        for num in lnums:
            root = avlArvore.insert(root, num)
            print(lnums)
            avlArvore.preOrder(root)
            print()
            print("===============")
                #raiz = root
            #return gui()
        '''else:
            for num in lnums:
                root = myTree.insert(root, num)
                print(lnums)
                myTree.preOrder(root)
                raiz = root
                print()
                print("===============")
            #return gui()'''
    if Op == 2:
        num = input("Remover número: ")
        # Delete
        #val = 10
        root = avlArvore.delete(root, num)
        print(lnums)
        #print("Preordem-")
        avlArvore.preOrder(root)
        print()
        #raiz = root
        print("===============")
        #return gui()
    if Op == 3:
        print(lnums)
        print(avlArvore.preOrder(root))
        print()
        print("==============")
    return gui()
gui()
