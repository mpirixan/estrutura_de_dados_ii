class TabelaHash:
    def __init__(self):
        self.tamanho = 11
        self.slots = [None] * self.tamanho
        self.dados = [None] * self.tamanho

    def put(self, val, data):
      hashval = self.funcaohash(val, len(self.slots))

      if self.slots[hashval] == None:
        self.slots[hashval] = val
        self.dados[hashval] = data
      else:
        if self.slots[hashval] == val:
          self.dados[hashval] = data  #substituição
        else:
          proxslot = self.rehash(hashval, len(self.slots))
          while self.slots[proxslot] != None and \
                          self.slots[proxslot] != val:
            proxslot = self.rehash(proxslot, len(self.slots))

          if self.slots[proxslot] == None:
            self.slots[proxslot]=val
            self.dados[proxslot]=data
          else:
            self.dados[proxslot] = data #substituição

    def funcaohash(self, val, tamanho):
         return val % tamanho

    def rehash(self, hashantigo, tamanho):
        return (hashantigo + 1) % tamanho

    def get(self, val):
      startslot = self.funcaohash(val, len(self.slots))

      dados = None
      stop = False
      found = False
      posicao = startslot
      while self.slots[posicao] != None and  \
                           not found and not stop:
         if self.slots[posicao] == val:
           found = True
           dados = self.dados[posicao]
         else:
           posicao=self.rehash(posicao,len(self.slots))
           if posicao == startslot:
               stop = True
      return dados

    def __getitem__(self, val):
        return self.get(val)

    def __setitem__(self, val, data):
        self.put(val, data)

H=TabelaHash()
H[54]="gato"
H[26]="cachorro"
H[93]="tigre"
H[17]="bagre"
H[77]="tucunare"
H[31]="vaca"
H[44]="cabra"
H[55]="boi"
H[20]="tartaruga"
print("Slots", (H.slots))
print("Valores", (H.dados))

print("20:", (H[20]))

print("17:", (H[17]))
print("Inserção 20 = Pato")
H[20]='pato'
print("20:", (H[20]))
print("99:", (H[99]))
print("Slots", (H.slots))
print("Valores", (H.dados))
