from AVL1 import AVL_Tree
def gui():
    global root
    global lnums
    avlArvore = AVL_Tree()
    menu = """
     ---Menu de Escolha--
    [1] - Insere
    [2] - Remove
    """
    print(menu)
    Op = int(input('==> '))
    if Op == 1:
        root = None
        num = input("Insira o número: ")
        lnums.append(num)
        for num in lnums:
            root = avlArvore.insert(root, num)
            print(lnums)
            print("Preordem-Raiz,esquerda,direita")
            avlArvore.preOrder(root)
            print()
            print("===============")

    if Op == 2:
        num = input("Remover número: ")
        root = avlArvore.delete(root, num)
        print(lnums)
        print("Preordem-Raiz,esquerda,direita")
        avlArvore.preOrder(root)
        print()
        print("===============")

    return gui()
gui()