# import numpy as np

class MinHeap:
    def __init__(self):
        self.heap_lista = [0]
        self.tamanho_atual = 0
    
    def sobe(self, i):
        while i // 2 > 0:
            if self.heap_lista[i] < self.heap_lista[i // 2]:
                self.heap_lista[i], self.heap_lista[i // 2] = self.heap_lista[i // 2], self.heap_lista[i]
            i = i // 2
    def insere(self, k):

        self.heap_lista.append(k)

        self.tamanho_atual += 1 

        self.sobe(self.tamanho_atual)
    
    def desce(self, i):

        while (i * 2) <= self.tamanho_atual:
            fm = self.filho_min(i)

            if self.heap_lista[i] > self.heap_lista[fm]:
                self.heap_lista[i], self.heap_lista[fm] = self.heap_lista[fm], self.heap_lista[i]
            i = fm
    
    def filho_min(self,i): 
        if (i * 2) + 1 > self.tamanho_atual:
            return i * 2
        else:

            if  self.heap_lista[i * 2] < self.heap_lista[(i*2)+1]:
                return i * 2
            else:
                return (i * 2) + 1

    def deleta_min(self):

        if len(self.heap_lista) == 1:
            return 'Heap vazio'

        raiz = self.heap_lista[1]

        self.heap_lista[1] = self.heap_lista[self.tamanho_atual]

        *self.heap_lista, _ = self.heap_lista 

        self.tamanho_atual -= 1 

        self.desce(1)

        return raiz
heap = MinHeap()

heap.insere(5)
heap.insere(7)
heap.insere(9)
heap.insere(1)
heap.insere(3)
heap.insere(10)
heap.insere(8)
heap.insere(4)
print(heap.heap_lista)
print("Fora:", heap.deleta_min())
print(heap.heap_lista)
print("Fora:", heap.deleta_min())
print(heap.heap_lista)
print("Fora:", heap.deleta_min())
print(heap.heap_lista)
print("Fora:", heap.deleta_min())
print(heap.heap_lista)
print("Fora:", heap.deleta_min())
print(heap.heap_lista)
print("Fora:", heap.deleta_min())
print(heap.heap_lista)
print("Fora:", heap.deleta_min())
print(heap.heap_lista)