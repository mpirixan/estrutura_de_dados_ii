import random
import time
from os import system, name

civil = [' 100000 ' ,' 100001 ' ,' 100002 ' ,' 100003 ' ,' 100004 ' ,' 100005 ' ,' 100006 ' ,' 100007 ' ,' 100008 ' ,
         ' 100009 ' ,' 100010 ' ,' 100011 ' ,' 100012 ' ,' 100013 ' ,' 100014 ' ,' 100015 ' ,' 100016 ' ,' 100017 ' ,
         ' 100018 ' ,' 100019 ']

policial = [' 100020 ' ,' 100021 ' ,' 100022 ' ,' 100023 ' ,' 100024 ' ,' 100025 ' ,' 100026 ' ,' 100027 ' ,' 100028 ' ,
            ' 100029 ' ,' 100030 ' ,' 100031 ' ,' 100032 ' ,' 100033 ' ,' 100034 ' ,' 100035 ' ,' 100036 ' ,' 100037 ' ,
            ' 100038 ' ,' 100039 ' ,]

bombeiro = [' 100040 ' ,' 100041 ' ,' 100042 ' ,' 100043 ' ,' 100044 ' ,' 100045 ' ,' 100046 ' ,' 100047 ' ,' 100048 ' ,
            ' 100049 ' ,' 100050 ' ,' 100051 ' ,' 100052 ' ,' 100053 ' ,' 100054 ' ,' 100055 ' ,' 100056 ' ,' 100057 ' ,
            ' 100058 ' ,' 100059 ' ,]

medico = [' 100060 ' ,' 100061 ' ,' 100062 ' ,' 100063 ' ,' 100064 ' ,' 100065 ' ,' 100066 ' ,' 100067 ' ,' 100068 ' ,
        ' 100069 ' ,' 100070 ' ,' 100071 ' ,' 100072 ' ,' 100073 ' ,' 100074 ' ,' 100075 ' ,' 100076 ' ,' 100077 ' ,
        ' 100078 ' ,' 100079 ' ,]

antena1 = []
antena2 = []
antena3 = []
antena4 = []
intobj = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
fila1 = []
listaocupados=[]
prioridade = [civil, policial, bombeiro, medico]
progam = 1
intantena = [antena4, antena3, antena2, antena1]

def verificaantenas():
    if len(antena1) < 4:
        return antena1
    elif len(antena2) < 4:
        return antena2
    elif len(antena3) < 4:
        return antena3
    elif len(antena4) < 4:
        return antena4
    else:
        return fila1

#TODO verificação de fila
#def verificafila():

def ligacao():
    print("Ligacao...")

    # Escolha de usuario1
    usuarioc1 = random.choice(prioridade)
    numeroc1 = random.choice(intobj)

    # Escolha de usuario2
    usuarioc2 = random.choice(prioridade)
    numeroc2 = random.choice(intobj)

    #Define as Prioridades
    prioval1 = prioval(usuarioc1)
    prioval2 = prioval(usuarioc2)
    sumPrioridade = prioval1 + prioval2

    # Escolhe antena
    antena = random.choice(intantena)



    if len(fila1) > 1:
        fimligacao = random.choice(intantena)
        if len(fimligacao) < 1:
            print("")
        else:
            print("Fim de ligação na antena: ", fimligacao)
            print("Numero excluido: ", fimligacao.pop(0))
            print("Vagas na antena: ", fimligacao)
            print("=============================")


    if len(antena) == 4:
        fila1.append(sumPrioridade)
        heap(fila1)
        print("Prioridade adicionada na fila:", sumPrioridade)
        print("fila: ", fila1)

    elif len(antena) < 4 and len(fila1) > 1:
        novonumero = fila1.pop(0)
        print("Tirando da fila: ", novonumero)
        heap(fila1)
        print("Adicionando ",novonumero ," na antena: ",antena)
        antena.append(novonumero)
    else:
        antena.append(sumPrioridade)
        print("usuario numero:", numeroc1, "Ligou para usuario numero:", numeroc2)
    print("\n")
#TODO verificar usuário ocupado
#def verificauser():

def clear():
    # for windows
    if name == 'nt':
        _ = system('cls')

    # for mac and linux(here, os.name is 'posix')
    else:
        _ = system('clear')

def gui():
    print("==============================")
    print('-----ANTENAS-----')
    print('==============================')
    print("Antena 1:", antena1)
    print("Antena 2:", antena2)
    print("Antena 3:", antena3)
    print("Antena 4:", antena4)
    print("Fila de Espera:", fila1)
    print("===============================")
    print("\n")


def prioval(name):
    if name == civil:
        return 1
    elif name == policial:
        return 2
    elif name == bombeiro:
        return 3
    else:
        return 4



def maxHeapfy(list, raiz, tam_lista):
    array = list
    maior = raiz

    # buscando os filhos direito e esquerdo
    filho_esq = 2 * raiz + 1
    filho_dir = 2 * raiz + 2

    # verificando se a subárvore é um heap
    if filho_esq < tam_lista and array[filho_esq] > array[maior]:
        aux = maior
        maior = filho_esq
        filho_esq = aux
    if filho_dir < tam_lista and array[filho_dir] > array[maior]:
        aux = maior
        maior = filho_dir
        filho_dir = aux

    # efetuando as modificações após as verificações
    if array[maior] != array[raiz]:
        aux = array[raiz]
        array[raiz] = array[maior]
        array[maior] = aux

        maxHeapfy(array, maior, tam_lista)


def heap(list):
    list = fila1
    raiz = (len(fila1) // 2) - 1
    while raiz >= 0:
        maxHeapfy(fila1, raiz, len(fila1))
        raiz -= 1
    return fila1



while progam < 100:
    # Executa Ligação
    ligacao()
    # Printa as antenas
    gui()
    time.sleep(5.5)
    progam += 1
    clear()
